from terminal_caller import TerminalCaller
from pakku_caller import pakku_caller
terminal = TerminalCaller()


class Switches:
    def __init__(self, conf, option):
        self.conf = conf
        self.option = option

    def toggle(self, switch, param):
        state = switch.get_active()
        return self.updateConfig(state)

    def updateConfig(self, state):
        return self.conf.modify(self.option, str(state))


class Button:
    def __init__(self, option, config, entry=None):
        self.option = option
        self.entry = entry
        self.config = config

    def click(self, button, entry=None):
        if entry is not None:
            text = entry.get_text()
        elif self.entry is not None:
            text = self.entry.get_text()
        else:
            text = ""
        return self.call(self.option, text)

    def call(self, option, packages=""):
        arguments = self.config.getOptions()
        cmd = pakku_caller(option, packages, arguments)
        terminal.start(cmd.string())


class Entry:
    def __init__(self, option):
        self.option = option

    def activate(self, entry, btn):
        return btn.click(None, entry)
