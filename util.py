def isTrue(item):
    return item.lower() in ["true", "y", "t", "1"]
